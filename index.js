'use strict';
const PDFGenerator = require('html-pdf');
const fs = require('fs');

// HTML STRING
const htmlString = fs.readFileSync('./template.html', 'utf8');

// PDF OPTIONS.
const pdfOptions = {
  format: 'A4'
};

// This will responsible to generate the PDF from HTML template
PDFGenerator.create(htmlString, pdfOptions).toFile('./demo.pdf', (error, success) => {
  if (error) {
    console.error('An error occurred while generating the PDF from HTML');
  } else {
    console.info('PDF successfully generated from HTML', success);
  }
})

